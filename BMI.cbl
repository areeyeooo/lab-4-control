       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL3.
       AUTHOR. AREEYEO.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT_KG PIC 999v99.
       01  HEIGHT_IN PIC 999v99.
       01  BMI    PIC 99V99.
           88  UNDERWEIGHT    VALUE ZERO THRU 18.4.
           88  NORMALWEIGHT   VALUE 18.5 THRU 24.9.
           88  OVERWEIGHT     VALUE 25   THRU 29.9.
           88  OBESE          VALUE 30   THRU 34.9.
           88  EXTERMLY-OBESE VALUE 35   THRU 99.
      
       PROCEDURE DIVISION.
       BEGIN.           
           DISPLAY "Input height in In.: " WITH NO ADVANCING
           ACCEPT HEIGHT_IN.
           DISPLAY "Input weight in Kg.: " WITH NO ADVANCING
           ACCEPT WEIGHT_KG.

           COMPUTE BMI = WEIGHT_KG / (HEIGHT_IN * HEIGHT_IN ) .
      *    DISPLAY "The BMI is: ", BMI, "%".
           IF UNDERWEIGHT THEN
           DISPLAY "The BMI is: ", BMI, "% (Underweight)"
           END-IF 
           IF NORMALWEIGHT THEN
           DISPLAY "The BMI is: ", BMI, "% (Normal)"
           END-IF 
           IF OVERWEIGHT THEN
           DISPLAY "The BMI is: ", BMI, "% (Overweight)"
           END-IF 
           IF OBESE THEN
           DISPLAY "The BMI is: ", BMI, "% (Obese)"
           END-IF
           IF EXTERMLY-OBESE THEN
           DISPLAY "The BMI is: ", BMI, "% (Extermly Obese)"
           END-IF 
           GOBACK 
           .

