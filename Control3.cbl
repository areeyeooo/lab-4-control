       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL3.
       AUTHOR. AREEYEO.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  CITY-CODE   PIC 9 VALUE ZERO.
           88 CITY-IS-DUBLIN       VALUE 1.
           88 CITY-IS-LEMIRICK     VALUE 2.
           88 CITY-IS-CORK         VALUE 3.
           88 CITY-IS-GALWAY       VALUE 4.
           88 CITY-IS-SLIGO        VALUE 5.
           88 CITY-IS-WATERFORD    VALUE 6.
           88 UNIVERSITY-CITY      VALUE 1 THRU 4.
           88 CITY-CODE-NOT-VALID  VALUE 0, 7, 8, 9.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "ENTER A CITY CODE (1-6) - " WITH NO ADVANCING 
           ACCEPT CITY-CODE 

           IF CITY-CODE-NOT-VALID THEN
              DISPLAY "INVALID CITY CODE ENTERED"
           ELSE
              IF CITY-IS-LEMIRICK THEN
              DISPLAY "HEY, WE'RE HOME."
              END-IF 
              IF CITY-IS-DUBLIN  THEN
              DISPLAY "HEY, WE'RE THE CAPITAL."
              END-IF 
              IF UNIVERSITY-CITY THEN
              DISPLAY "APPLY THE RENT SURCHARGE!."
              END-IF 
              SET CITY-IS-DUBLIN TO TRUE 
              DISPLAY CITY-CODE 
              GOBACK 
           .
